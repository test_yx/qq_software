#include "TCPKernel.h"
#include "TCPNet.h"

TCPKernel::TCPKernel(void)
{
	m_pNet = new TCPNet(this);
}

TCPKernel::~TCPKernel(void)
{
	if(m_pNet)
	{
		delete m_pNet;
		m_pNet = NULL;
	}
}

bool TCPKernel::Open()
{
	if(!m_sql.ConnectMySql("localhost","root","123456","qq_kelin"))
		return false;

	if(!m_pNet->InitNetWork())
		return false;

	return true;
}
void TCPKernel::Close()
{
	m_sql.DisConnect();
	m_pNet->UnInitNetWork();
}

void TCPKernel::Register(char* szbuf,SOCKET sockWaiter)
{//申请qq号
	
	STRU_REGISTER_RQ *psrr = (STRU_REGISTER_RQ *)szbuf;
	list<string> lststr;
	STRU_REGISTER_RS srr;
	srr.m_nType = _DEFFAULT_PROTOCOL_REGISTER_RS;
	srr.m_qq = 0;
	char szsql[SQLLEN] = {0};
	sprintf_s(szsql,"insert into user(u_tel,u_name,u_password) values(%lld,'%s','%s')",
		psrr->m_tel,psrr->m_szName,psrr->m_szPassword);
	if(m_sql.UpdateMySql(szsql))
	{//获取QQ号
		sprintf_s(szsql,"select u_qq from user where u_tel = %lld",psrr->m_tel);
		m_sql.SelectMySql(szsql,1,lststr);//查看
		if(lststr.size() >0)//判断
		{
			string strQQ = lststr.front();
			lststr.pop_front();
			srr.m_qq  = _atoi64(strQQ.c_str());//类型转换
			//用户状态
			sprintf_s(szsql,"insert into userstate(u_qq)  values(%lld)",srr.m_qq );
			m_sql.UpdateMySql(szsql);
		}	
	}
	//回复
	m_pNet->SendData(sockWaiter,(char*)&srr,sizeof(srr));
}

void TCPKernel::Login(char* szbuf,SOCKET sockWaiter)
{
	//取包
	STRU_LOGIN_RQ *pslr = (STRU_LOGIN_RQ*)szbuf;
	char szsql[SQLLEN] = {0};
	STRU_LOGIN_RS slr;
	slr.m_nType = _DEFFAULT_PROTOCOL_LOGIN_RS;
	slr.m_szResult = _login_noexist_user;
	list<string> lststr;
	sprintf_s(szsql,"select u_password from user where u_qq = %lld",pslr->m_qq);
	m_sql.SelectMySql(szsql,1,lststr);
	if(lststr.size() >0)
	{
		slr.m_szResult = _login_err_userpassword;
		string strpassword = lststr.front();
		lststr.pop_front();
		if( 0 == strcmp(pslr->m_szPassword,strpassword.c_str()))
		{
			slr.m_szResult = _login_success;
			sprintf_s(szsql,"update userstate set u_datetime =now(),u_state = 1 where u_qq = %lld;",pslr->m_qq);
			m_sql.UpdateMySql(szsql);
			m_mapQQToSocket[pslr->m_qq] =sockWaiter;
		}
	}
	//发送回复
	m_pNet->SendData(sockWaiter,(char*)&slr,sizeof(slr));
}

void TCPKernel::GetFriendList(char* szbuf,SOCKET sockWaiter)
{
	STRU_GETFRIENDLIST_RQ *psgr = (STRU_GETFRIENDLIST_RQ*)szbuf;
	char szsql[SQLLEN] = {0};
	STRU_GETFRIENDLIST_RS sgr;
	sgr.m_nType = _DEFFAULT_PROTOCOL_GETFRIENDLIST_RS;
	list<string> lststr;//列表
	int i = 0;
	sprintf_s(szsql,"select uf.u_friendqq,u_name,u_state from user_friend uf \
inner join  user on uf.u_friendqq = user.u_qq \
inner join userstate on uf.u_friendqq = userstate.u_qq \
where uf.u_qq = %lld;",psgr->m_qq);
	m_sql.SelectMySql(szsql,3,lststr);
	while(lststr.size() >0)
	{
		string strFriendQQ = lststr.front();
		lststr.pop_front();
		string strFriendName = lststr.front();
		lststr.pop_front();
		string strFriendState = lststr.front();
		lststr.pop_front();
		sgr.m_aryFriendInfo[i].m_qq = _atoi64(strFriendQQ.c_str());
		strcpy_s(sgr.m_aryFriendInfo[i].m_szName,MAXSIZE,strFriendName.c_str());
		sgr.m_aryFriendInfo[i].m_szState = *strFriendState.c_str();
		i++;
		if(i == FRIENDNUM || lststr.size() ==0)
		{
			sgr.m_nFriendNum = i;
			m_pNet->SendData(sockWaiter,(char*)&sgr,sizeof(sgr));
			i =0;
		}
	}
}

void TCPKernel::SelectFriendChat(char* szbuf,SOCKET sockWaiter)
{
	STRU_SELECTFRIENDCHAT_RQ *pssr = (STRU_SELECTFRIENDCHAT_RQ*)szbuf;
	SOCKET sockFriend =  m_mapQQToSocket[pssr->m_Friendqq];
	if(sockFriend)
	{
		m_pNet->SendData(sockFriend,szbuf,sizeof(STRU_SELECTFRIENDCHAT_RQ));
	}
}

void TCPKernel::DealData(char* szbuf,SOCKET sockWaiter)//处理数据
{
	switch (*szbuf)
	{
	case _DEFFAULT_PROTOCOL_REGISTER_RQ:
		{
			Register(szbuf,sockWaiter);
		}
		break;
	case _DEFFAULT_PROTOCOL_LOGIN_RQ:
		{
			Login(szbuf,sockWaiter);
		}
		break;
	case _DEFFAULT_PROTOCOL_GETFRIENDLIST_RQ:
		{
			GetFriendList(szbuf,sockWaiter);
		}
		break;
	case _DEFFAULT_PROTOCOL_SELECTFRIENDCHAT_RQ:
		{
			SelectFriendChat(szbuf,sockWaiter);
		}
		break;
	default:
		break;
	}
}