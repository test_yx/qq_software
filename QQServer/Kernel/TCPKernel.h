#pragma once 

#include "IKernel.h"
#include "INet.h"
#include "CMySql.h"
#include "Packdef.h"
#include <map>
class TCPKernel :public IKernel
{
public:
	TCPKernel(void);

	~TCPKernel(void);
public:
	 bool Open();
	 void Close();
	 void DealData(char* szbuf,SOCKET sockWaiter);
public:
	 void Register(char* szbuf,SOCKET sockWaiter);
	 void Login(char* szbuf,SOCKET sockWaiter);
	 void GetFriendList(char* szbuf,SOCKET sockWaiter);
	 void SelectFriendChat(char* szbuf,SOCKET sockWaiter);
private:
	INet *m_pNet;
	CMySql m_sql;
	std::map<long long,SOCKET > m_mapQQToSocket;
};