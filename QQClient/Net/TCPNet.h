#pragma once 
#include "INet.h"

class TCPNet :public INet
{
public:
	TCPNet(void);
	~TCPNet(void);
public:
	bool InitNetWork();
	void UnInitNetWork();
	bool SendData(char *szbuf,int nLen);
	void RecvData();
public:
	static unsigned  __stdcall ThreadProc( void * );
private:
	SOCKET m_sockClient;
	HANDLE m_hThread;
	bool   m_bFlagQuit;
};