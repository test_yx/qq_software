#pragma once 

#include "IKernel.h"
#include "INet.h"
class TCPKernel:public IKernel
{
public:
	TCPKernel(void);
	~TCPKernel(void);
public:
	 bool Open();
	 void Close();
	 void DealData(char* szbuf) ;
	 bool SendData(char *szbuf,int nLen);
private:
	INet *m_pNet;
};

