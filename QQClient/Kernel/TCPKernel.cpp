#include "stdafx.h"
#include "TCPKernel.h"
#include "TCPNet.h"
#include "0114QQClient.h"
TCPKernel::TCPKernel(void)
{
	m_pNet = new TCPNet;
}
TCPKernel::	~TCPKernel(void)
{
	delete m_pNet;
	m_pNet = NULL;
}

bool TCPKernel::Open()
{
	if(!m_pNet->InitNetWork())
		return false;
	return true;
}

void TCPKernel::Close()
{
	m_pNet->UnInitNetWork();
}

void TCPKernel::DealData(char* szbuf) 
{
	switch(*szbuf)
	{
	case _DEFFAULT_PROTOCOL_REGISTER_RS:
		{
			STRU_REGISTER_RS *psrr = (STRU_REGISTER_RS*)szbuf;
			if(psrr->m_qq)
				theApp.m_pMainWnd->SendMessageA(UM_REGISTERMSG,(WPARAM)&psrr->m_qq);
			else
			AfxMessageBox("QQע��ʧ����");
		}
		break;
	case _DEFFAULT_PROTOCOL_LOGIN_RS:
		{
			STRU_LOGIN_RS *pslr=(STRU_LOGIN_RS*)szbuf;
			char *pSzResult="_login_err_userpassword";
			if(pslr->m_szResult==_login_success)
			{
				theApp.m_pMainWnd->PostMessage(UM_LOGINMSG);
				return;
			}
			else if(pslr->m_szResult==_login_noexist_user)
				pSzResult="_login_noexist_user";
			AfxMessageBox(pSzResult);
		}
		break;
		case _DEFFAULT_PROTOCOL_GETFRIENDLIST_RS:
		{
			STRU_GETFRIENDLIST_RS  *psgr = (STRU_GETFRIENDLIST_RS*)szbuf;
			theApp.m_pMainWnd->SendMessage(UM_FRIENDLISTMSG,(WPARAM)psgr->m_aryFriendInfo,psgr->m_nFriendNum);
		}
		break;
	case _DEFFAULT_PROTOCOL_SELECTFRIENDCHAT_RQ:
		{
			STRU_SELECTFRIENDCHAT_RQ *pssr = (STRU_SELECTFRIENDCHAT_RQ*)szbuf;
			theApp.m_pMainWnd->SendMessage(UM_SELECTFRIENDCHATMSG,(WPARAM)pssr->m_qq,(LPARAM)&pssr->m_qq);
		}
		break;
	}
}

bool TCPKernel::SendData(char *szbuf,int nLen)
{
	if(!m_pNet->SendData(szbuf,nLen))
		return false;
	return true;
}