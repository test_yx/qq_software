// CdlgMain.cpp : 实现文件
//

#include "stdafx.h"
#include "0114QQClient.h"
#include "CdlgMain.h"
#include "afxdialogex.h"


// CdlgMain 对话框

IMPLEMENT_DYNAMIC(CdlgMain, CDialogEx)

CdlgMain::CdlgMain(CWnd* pParent /*=NULL*/)
	: CDialogEx(CdlgMain::IDD, pParent)
{

}

CdlgMain::~CdlgMain()
{
}

void CdlgMain::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, s_lstcrl);
}


BEGIN_MESSAGE_MAP(CdlgMain, CDialogEx)
	ON_MESSAGE(UM_FRIENDLISTMSG,&CdlgMain::GetFriendListRs)
	ON_MESSAGE(UM_SELECTFRIENDCHATMSG,&CdlgMain::SelectFriendChatRq)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, &CdlgMain::OnNMDblclkList1)
END_MESSAGE_MAP()


// CdlgMain 消息处理程序
LRESULT CdlgMain::GetFriendListRs(WPARAM W,LPARAM L)
{
	FriendInfo *pInfo = (FriendInfo*)W;
	CString str;
	for(int i =0; i<L;i++)
	{
		//qq
		str.Format(_T("%lld"),pInfo[i].m_qq);
		s_lstcrl.InsertItem(i,str);
		//name
		s_lstcrl.SetItemText(i,1,pInfo[i].m_szName);
		//state
		str.Format(_T("%c"),pInfo[i].m_szState);
		s_lstcrl.SetItemText(i,2,str);
	}
	return 0;
}

LRESULT CdlgMain::SelectFriendChatRq(WPARAM W,LPARAM L)
{
	char *szbuf  = (char*)W;
	CString  strQQ;
	long long qq =*(long long *)L;
	strQQ.Format(_T("%lld"),qq);
	CDlgChat  *pDlg  = m_mapQQToDlg[strQQ];
	//弹出聊天窗口
	if(!pDlg)
	{
		pDlg = new CDlgChat;
		pDlg->Create(IDD_DLG_CHAT);
		pDlg->SetWindowTextA(strQQ);
		m_mapQQToDlg[strQQ] = pDlg;
	}
	pDlg->ShowWindow(SW_SHOW);
	//显示聊天信息
	strQQ += "say:";
	pDlg->m_lstbox.AddString(strQQ);
	pDlg->m_lstbox.AddString(szbuf);
	return 0;
}

BOOL CdlgMain::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	s_lstcrl.InsertColumn(0,_T("QQ号"),LVCFMT_LEFT,50);
	s_lstcrl.InsertColumn(1,_T("昵称"),LVCFMT_LEFT,100);
	s_lstcrl.InsertColumn(2,_T("状态"),LVCFMT_LEFT,50);

	s_lstcrl.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	STRU_GETFRIENDLIST_RQ sgr;
	sgr.m_nType = _DEFFAULT_PROTOCOL_GETFRIENDLIST_RQ;
	sgr.m_qq = m_qq;

	theApp.m_pKernel->SendData((char*)&sgr,sizeof(sgr));
	return TRUE;  
}


void CdlgMain::OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	CString strQQ  = s_lstcrl.GetItemText(pNMItemActivate->iItem,0);
	CDlgChat  *pDlg  = m_mapQQToDlg[strQQ];
	//弹出聊天窗口
	if(!pDlg)
	{
		pDlg = new CDlgChat;
		pDlg->Create(IDD_DLG_CHAT);
		pDlg->SetWindowTextA(strQQ);
		m_mapQQToDlg[strQQ] = pDlg;
	}
	pDlg->ShowWindow(SW_SHOW);
}