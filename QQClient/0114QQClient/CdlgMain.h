#pragma once
#include "afxcmn.h"
#include "DlgChat.h"
#include <map>

// CdlgMain 对话框

class CdlgMain : public CDialogEx
{
	DECLARE_DYNAMIC(CdlgMain)

public:
	CdlgMain(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CdlgMain();

// 对话框数据
	enum { IDD = IDD_DLG_MAIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	LRESULT GetFriendListRs(WPARAM W,LPARAM L);
	LRESULT SelectFriendChatRq(WPARAM W,LPARAM L);
	DECLARE_MESSAGE_MAP()
public:
	long long m_qq;

	virtual BOOL OnInitDialog();
	CListCtrl s_lstcrl;
	std::map<CString,CDlgChat*>m_mapQQToDlg;
	afx_msg void OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult);
};
