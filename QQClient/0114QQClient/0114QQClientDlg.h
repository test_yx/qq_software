
// 0114QQClientDlg.h : 头文件
//

#pragma once

// CMy0114QQClientDlg 对话框
class CMy0114QQClientDlg : public CDialogEx
{
// 构造
public:
	CMy0114QQClientDlg(CWnd* pParent = NULL);	// 标准构造函数
// 对话框数据
	enum { IDD = IDD_MY0114QQCLIENT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedButton1();
	LRESULT RegisterRs(WPARAM W,LPARAM L);
	LRESULT LoginRs(WPARAM W,LPARAM L);
	DECLARE_MESSAGE_MAP()
public:
	CString m_edtusername;
	CString m_edtpassword;
	LONGLONG m_edtqq;
	LONGLONG m_edttel;
	afx_msg void OnEnChangeEdtPassword();
	afx_msg void OnBnClickedButton2();
};
